/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"pwc/Example/test/unit/AllTests"
	], function () {
		QUnit.start();
	});
});